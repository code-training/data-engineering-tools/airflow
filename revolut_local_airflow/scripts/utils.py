from contextlib import contextmanager

@contextmanager
def session_scope(Session):
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


import yaml

def read_yaml(path):
    with open(path) as file:
        return yaml.safe_load(file)


