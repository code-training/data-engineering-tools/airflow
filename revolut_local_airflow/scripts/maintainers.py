from glob import glob
import os
from itertools import chain
from airflow.utils.log.logging_mixin import LoggingMixin

logger = LoggingMixin()

def listdir_nohidden(path):
    return glob(os.path.join(path, '*'))
    #glob(os.path.join(path, '**/'), recursive=True)

def find_maintainers(path_folder, maintainers_file = None):

    path_files = {
        f for f in listdir_nohidden(path_folder) 
        if f.endswith('maintainers.yaml') or 
        (os.path.isdir(f) and not f.endswith('__pycache__'))
    }

    path_yaml = os.path.join(path_folder, 'maintainers.yaml')
    if path_yaml in path_files:
        path_files.remove(path_yaml)
        maintainers_file = path_yaml
    else: 
        logger.log.info('Folder {0} does not have maintainers.yaml'.format(path_folder))

    if len(path_files) == 0:
        return [{'folder': path_folder, 'maintainers_yaml': maintainers_file }]

    _list  = [find_maintainers(path_file, maintainers_file) for path_file in path_files]
    return list(chain(*_list))

def clean_maintainers(maintainer):
    if maintainer.get('maintainers_yaml', None) is None:
        return False
    return True


def get_maintainers(path_folder_dags):
    
    folders = [p for p in listdir_nohidden(path_folder_dags) if not p.endswith('.py')]

    maintainers = list()
    for folder in folders:
        result = find_maintainers(folder)
        maintainers.extend(result)

    maintainers =  filter(clean_maintainers, maintainers)
    maintainers = [ 
        { 
            'folder': m['folder'].replace(path_folder_dags, '').replace('/', '', 1),
            'maintainers_yaml': m['maintainers_yaml']
        } 
        for m in maintainers
    ]
    
    return maintainers

