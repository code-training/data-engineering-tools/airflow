from airflow import settings
from airflow.utils.db import create_session
from airflow.utils.state import State
from airflow.models import DagBag
from flask_appbuilder.security.sqla import models
from airflow.utils.log.logging_mixin import LoggingMixin
#https://github.com/apache/airflow/tree/1.10.12/airflow/models

from os import path

# print(settings.DAGS_FOLDER)
# print(settings.STORE_SERIALIZED_DAGS)
# dagbag = DagBag(settings.DAGS_FOLDER, store_serialized_dags=settings.STORE_SERIALIZED_DAGS)

# _path = path.join(settings.DAGS_FOLDER,'acquiring/')

# d = DagBag(_path, store_serialized_dags=settings.STORE_SERIALIZED_DAGS)
# print(_path)
# print(list(d.dag_ids))
# print(d.get_dag('ac1'))

LoggingMixin().log.info("Hello")
import logging


logger = logging.getLogger("airflow.task")
logger.error("Your custom error")


logger = logging.getLogger("rbac.maintainers")


def read_yaml(path):
    with open(path) as file:
        return yaml.safe_load(file)


def list_maintainers(path):
    return glob(os.path.join(path, '**/maintainers.yaml'), recursive=True)


def get_maintainers(path_folder_dags):
    for path_yaml in list_maintainers(path_folder_dags):
        if os.path.exists(path_yaml):
            maintainers = read_yaml(path_yaml)
            group_name = str(Path(path_yaml).relative_to(path_folder_dags).parent)
            yield group_name, maintainers
