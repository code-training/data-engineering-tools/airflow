
from os import path, mkdir
import shutil

directories = ['dir_1', 'dir_2', 'dir_3']


def create_directory(dir):
    if not path.exists(dir):
        mkdir(dir)

def create_yaml(dir):
    if path.exists(dir):
        open(path.join(dir, 'maintainers.yaml'), 'a').close()


def remove_directory(dir):
    if path.exists(dir):
        shutil.rmtree(dir)


def create_all(path_dags):
    create_directory(path_dags)
    [create_directory(path.join(path_dags, d)) for d in directories]
    [create_yaml(path.join(path_dags, d)) for d in directories]


    path_dir_4 = path.join(path_dags, 'dir_4')
    create_directory(path_dir_4)
    create_directory(path.join(path_dir_4, 'dir_4_int_1'))
    create_yaml(path.join(path_dir_4, 'dir_4_int_1'))
    create_directory(path.join(path_dir_4, 'dir_4_int_2'))
    create_yaml(path.join(path_dir_4, 'dir_4_int_2'))
    create_directory(path.join(path_dir_4, 'dir_4_int_3'))
    