import re
import sqlite3
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy.orm import sessionmaker
from flask_appbuilder.security.sqla.models import (
    Permission,
    Role,
    User,
    assoc_user_role,
    ViewMenu,
    PermissionView,
    assoc_permissionview_role
)

from sqlite3 import Error
from os import getcwd, path, remove
from contextlib import contextmanager


@contextmanager
def session_scope(Session):
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()

class MockDatabase():

    def __init__(self, name_db):
        self.name_db = name_db
        self.delete_db()
        self.create_db()


    def get_connection(self):
        return f'sqlite:///{self.name_db}'


    def create_db(self):
        db_file = path.join(getcwd(), self.name_db)

        if not path.exists(db_file):
            conn = None
            try:
                conn = sqlite3.connect(db_file)
                print("The db was created")
            except Error as e:
                print(e)
            finally:
                if conn:
                    conn.close()

    def delete_db(self):
        db_file = path.join(getcwd(), self.name_db)
        if path.exists(db_file):
            print("The db was removed")
            remove(db_file)
        else:
            print("The db does not exist")


    def create_tables(self, engine, tables):
        [t.__table__.create(engine, checkfirst=True) for t in tables ]
        

    def populate_tables(self):

        engine = create_engine(self.get_connection(), echo = False)
        meta = MetaData()

        tables = [ Permission, Role, User, ViewMenu, PermissionView]
        self.create_tables(engine=engine, tables=tables)
        assoc_user_role.create(engine, checkfirst=True)
        assoc_permissionview_role.create(engine, checkfirst=True)

        Session = sessionmaker(bind=engine)
        with session_scope(Session) as session:
            # Permissions
            can_read = Permission(id=1, name='can_dag_edit')
            can_edit= Permission(id=2, name='can_dag_read')

            #roles
            r1 = Role(id=1, name='User')
            r2 = Role(id=2, name='Viewer')
            r3 = Role(id=3, name='test-maintainers')

            #view_menu
            v1= ViewMenu(id=1, name='tutorial_dag_a')
            v2= ViewMenu(id=2, name='tutorial_dag_b')
            v3= ViewMenu(id=3, name='tutorial_dag_c')

            #users
            u1 = User(id=1, first_name='john', last_name='doe', username='john', email='john.doe@example.com', created_by_fk=1, changed_by_fk=1)
            u2 = User(id=2, first_name='jane', last_name='doe', username='jane', email='jane.doe@example.com', created_by_fk=1, changed_by_fk=1)
            u3 = User(id=3, first_name='joe', last_name='doe', username='joe', email='joe.doe@example.com', created_by_fk=1, changed_by_fk=1)
            u4 = User(id=4, first_name='jin', last_name='yew', username='jin', email='jin.yew@example.com', created_by_fk=1, changed_by_fk=1)

            #user_roles

            u_r1 = assoc_user_role.insert().values(id=1, user_id=1, role_id=1)
            u_r2 = assoc_user_role.insert().values(id=2, user_id=2, role_id=1)
            u_r3 = assoc_user_role.insert().values(id=3, user_id=3, role_id=1)
            u_r4 = assoc_user_role.insert().values(id=4, user_id=4, role_id=3)

            # permissions_view
            p_v_read_1 = PermissionView(id=1, permission_id=1, view_menu_id=1)
            p_v_edit_1 = PermissionView(id=2, permission_id=2, view_menu_id=1)

            p_v_read_2 = PermissionView(id=3, permission_id=1, view_menu_id=2)
            p_v_edit_2 = PermissionView(id=4, permission_id=2, view_menu_id=2)

            p_v_read_3 = PermissionView(id=5, permission_id=1, view_menu_id=3)
            p_v_edit_3 = PermissionView(id=6, permission_id=2, view_menu_id=3)


            # permission_view_roles
            # User Role
            p_v_r_1 = assoc_permissionview_role.insert().values(id=1, permission_view_id=1, role_id=1)
            p_v_r_2 = assoc_permissionview_role.insert().values(id=2, permission_view_id=2, role_id=1)

            p_v_r_3 = assoc_permissionview_role.insert().values(id=3, permission_view_id=3, role_id=1)
            p_v_r_4 = assoc_permissionview_role.insert().values(id=4, permission_view_id=4, role_id=1)

            p_v_r_5 = assoc_permissionview_role.insert().values(id=5, permission_view_id=5, role_id=1)
            p_v_r_6 = assoc_permissionview_role.insert().values(id=6, permission_view_id=6, role_id=1)

            # Viewer Role
            p_v_r_7 = assoc_permissionview_role.insert().values(id=7, permission_view_id=2, role_id=2)
            p_v_r_8 = assoc_permissionview_role.insert().values(id=8, permission_view_id=4, role_id=2)
            p_v_r_9 = assoc_permissionview_role.insert().values(id=9, permission_view_id=6, role_id=2)

            # Insert in Classes
            new = [ 
                can_edit, can_read, 
                r1, r2, r3,
                v1, v2, v3,
                u1, u2, u3, u4, 
                p_v_read_1, p_v_edit_1,
                p_v_read_2, p_v_edit_2,
                p_v_read_3, p_v_edit_3
            ]
            session.add_all(new)

            # Insert in Table Objects
            stmts = [
                u_r1, u_r2, u_r3, u_r4,
                p_v_r_1, p_v_r_2, p_v_r_3, p_v_r_4,
                p_v_r_5, p_v_r_6, p_v_r_7, p_v_r_8,
                p_v_r_9
            ]

            for stmt in stmts:
                session.execute(stmt)

            session.commit()


    

