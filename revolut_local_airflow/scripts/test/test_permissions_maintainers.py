
import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy.sql.expression import exists
from sqlalchemy import and_
from flask_appbuilder.security.sqla.models import (
    Permission,
    Role,
    User,
    assoc_user_role,
    ViewMenu,
    PermissionView,
    assoc_permissionview_role
)

from mock_database import MockDatabase, session_scope
from ..permissions_maintainers import add_role, add_permissions, attach_new_role_to_users, attach_permission_view_new_role

class TestPermissionMaintainer():
    db = MockDatabase(name_db="airflow.db")
    db.populate_tables()

    connection=db.get_connection()
    engine = create_engine(connection, echo = False)
    Session = sessionmaker(bind=engine, autoflush=True)

    def __del__(self):
        self.db.delete_db()


    def test_add_new_role(self):
        with session_scope(self.Session) as session:

            name_role = 'fake-team'
            result, id_result = add_role(session=session, name_role=name_role)
            assert result, 'Role not added'

            query = session.query(Role).filter(Role.name == name_role, Role.id == id_result)
            bool_result = session.query(query.exists()).scalar()
            assert bool_result, 'Role not inserted in the database'

    def test_get_id_existing_role(self):
        with session_scope(self.Session) as session:
            name_role = 'test-maintainers'
            result, id_result = add_role(session=session, name_role=name_role)
            id_expect = 3
            assert result
            assert id_result==id_expect, 'ID Role returned is not correct'

    def test_attach_new_role_to_users(self):
        mock_maintainers = [ 
            'john.doe@example.com',
            'jane.doe@example.com',
            'aaa@example.com',
            'jin.yew@example.com'
        ]

        with session_scope(self.Session) as session:
            user_roles_result = attach_new_role_to_users(session=session, id_role=3, maintainers=mock_maintainers)

            user_roles_expect = (
                (5,1,3),
                (6,2,3),
            )
            assert len(user_roles_result) == len(user_roles_expect), 'The lists are not equal'

            assert all([ True if u_r in user_roles_result else False for u_r in user_roles_expect]), 'Results do not have equal values'

    def test_attach_permission_view_new_role_validations(self):

        with session_scope(self.Session) as session:
            
            fake_view_dag = 'not_existing_dag'
            result = attach_permission_view_new_role(session=session, view_dag = fake_view_dag, id_permission=None, id_role = None)
            assert not result, 'Not validate if view menu exists'

            view_dag = 'tutorial_dag_a'
            id_permission = 3
            result = attach_permission_view_new_role(session=session, view_dag = view_dag, id_permission=id_permission, id_role = None)
            assert not result, 'Not validate if permission with view exists'

            view_dag = 'tutorial_dag_a'
            id_permission = 1
            id_role = 100
            result = attach_permission_view_new_role(session=session, view_dag = view_dag, id_permission=id_permission, id_role = id_role)
            assert not result, 'Not validate if role exists'

    def test_attach_permission_view_new_role(self):
        with session_scope(self.Session) as session:
            view_dag = 'tutorial_dag_a'
            id_permission = 1
            id_role = 3

            result = attach_permission_view_new_role(session=session, view_dag = view_dag, id_permission=id_permission, id_role = id_role)
            assert result, 'Not attach permission-views with roles'

            query = assoc_permissionview_role.select()\
                    .where(
                        and_(
                            assoc_permissionview_role.c.permission_view_id==1,
                            assoc_permissionview_role.c.role_id==3
                        )
                    )
            bool_result = session.execute(exists(query).select()).scalar()

            assert bool_result, 'New permission-view-role has not been added'


    def test_add_permissions(monkeypatch):

        assert True