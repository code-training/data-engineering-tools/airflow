
import os
from mock_maintainers import create_all, remove_directory
from ..maintainers import get_maintainers


class TestMaintainer():
    path_dags = os.path.join(os.getcwd(), 'dags')
    create_all(path_dags)

    def __del__(self):
        remove_directory(self.path_dags)

    def test_total_maintainers(self):

        maintainers = get_maintainers(self.path_dags)
        directories = ['dir_1', 'dir_2', 'dir_3', 'dir_4/dir_4_int_1', 'dir_4/dir_4_int_2' ]

        expected = [
            { 
                'folder': d, 
                'maintainers_yaml': os.path.join(self.path_dags, d, 'maintainers.yaml')
            } for d in directories
        ]

        assert len(maintainers) == len(expected)

        for e in expected:
            comparations =  []
            for m in maintainers:
                comparations.append(e == m)
                    
            assert any(comparations)

