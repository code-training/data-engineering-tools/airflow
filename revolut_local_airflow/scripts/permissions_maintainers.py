from os import getcwd, path
from airflow import settings
from airflow.models import DagBag
from sqlalchemy import and_
from sqlalchemy.sql.expression import exists
from airflow.utils.db import create_session
from airflow.utils.log.logging_mixin import LoggingMixin
from flask_appbuilder.security.sqla.models import (
    Permission,
    Role,
    User,
    assoc_user_role,
    ViewMenu,
    PermissionView,
    assoc_permissionview_role
)
#https://github.com/dpgaspar/Flask-AppBuilder/blob/master/flask_appbuilder/security/sqla/models.py

from maintainers import get_maintainers
from utils import read_yaml

logger = LoggingMixin()

def add_role(session, name_role):
    query = session.query(Role).filter(Role.name == name_role)
    if not session.query(query.exists()).scalar():
        role = Role(name= name_role)
        session.add(role)
        session.commit()
        logger.log.info(f'Add new role {name_role}')
        return True, role.id
    
    logger.log.info(f'Role {name_role} exists')
    return True, query.first().id


def attach_new_role_to_users(session, id_role, maintainers):

    attached = set()
    for maintainer in maintainers:
        
        user = session.query(User).\
            filter(User.email.ilike(f'%{maintainer}%')).\
            one_or_none()

        if bool(user):
            query = assoc_user_role.select()\
                .where(
                    and_(
                        assoc_user_role.c.user_id==user.id,
                        assoc_user_role.c.role_id==id_role
                    )
                )
            is_created = session.execute(exists(query).select()).scalar()      
            if not is_created:
                stmt = assoc_user_role.insert().values(user_id=user.id, role_id=id_role)
                result=session.execute(stmt)
                session.commit()
                attached.add((result.inserted_primary_key[0],user.id,id_role,))
            else:
                _,_,role = session.execute(query).fetchone()
                logger.log.info(f'User {maintainer} with role {role} exists')
        else:
            logger.log.info(f'User {maintainer} does not exists')

    return attached


def attach_permission_view_new_role(session, id_permission, view_dag, id_role):
    
    view = session.query(ViewMenu).filter( ViewMenu.name.ilike(f'%{view_dag}%')).one_or_none()
    if not bool(view):
        logger.log.info(f'View menu {view_dag} does not exist')
        return False

    permission_view = session.\
        query(PermissionView).\
        filter(
            PermissionView.permission_id == id_permission,
            PermissionView.view_menu_id == view.id
        ).one_or_none()

    if not bool(permission_view):
        logger.log.info(f' Permission with view {id_permission} does not exist')
        return False
        
    query = session.query(Role).filter(Role.id == id_role)
    if not session.query(query.exists()).scalar():
        logger.log.info(f'Role id {id_role} does not exist')
        return False

    role = query.first()
    
    # Attach new permission_view_role
    query = assoc_permissionview_role.select()\
        .where(
            and_(
                assoc_permissionview_role.c.permission_view_id==permission_view.id,
                assoc_permissionview_role.c.role_id==role.id
            )
        )
    is_created = session.execute(exists(query).select()).scalar()

    if not is_created:

        stmt = assoc_permissionview_role.insert().values(permission_view_id=permission_view.id, role_id=role.id)
        session.execute(stmt)
        session.commit()
        logger.log.info(f'Attach role "{role}" has permission to "{permission_view}"')
        return True
    else:
        logger.log.info(f'role "{role}" has permission to "{permission_view}" already exists')
        return None


def add_permissions(session, team):

    # Find the permission
    id_permission = session.query(Permission).filter(Permission.name.ilike(r'%can_dag_edit%')).first().id
    # Create new role
    name_role = f'{team.get("folder").lower()} maintainers'
    is_role_created, id_role = add_role(session=session, name_role=name_role)

    if is_role_created:
        
        # Connect new role with maintainers
        attach_new_role_to_users(
            session= session, 
            id_role=id_role, 
            maintainers = team.get('maintainers')
        )

        path_dags = path.join(settings.DAGS_FOLDER, team.get('folder'))
        dagbag = DagBag(
            path.join(settings.DAGS_FOLDER, path_dags), 
            store_serialized_dags=settings.STORE_SERIALIZED_DAGS
        )
        # Attach permissionView with role
        for dag_id in dagbag.dag_ids:
            attach_permission_view_new_role(
                session=session, id_permission=id_permission,
                view_dag=dag_id, id_role=id_role
            )

        return True
        
    return False

def run():

    path_folder_dags = r'/Users/alexander.angel/Desktop/repos/data/public-airflow-dags/'
    path_folder_dags = path.join(getcwd(),'..','dags')
    #path_maintainers = get_maintainers(path_folder_dags) #-- NEED IT
    #logger.log.info(path_maintainers)
    path_maintainers = [{'folder': 'acquiring', 'maintainers_yaml': '/root/airflow/scripts/../dags/acquiring/maintainers.yaml'}]

    for path_maintainer in path_maintainers:
        team = read_yaml(path=path_maintainer.get('maintainers_yaml'))
        team.update(path_maintainer)
        logger.log.info(team)
    
        with create_session() as session:
            # Add permissions to edit with views (dag pages) and roles
            add_permissions(session, team)


if __name__ == "__main__":
    run()
