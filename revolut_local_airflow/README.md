# Useful Commands

# Local

## Docker

> export DOCKER_DEFAULT_PLATFORM=linux/amd64

> docker run -e AIRFLOW**CORE**SQL_ALCHEMY_CONN='sqlite:////root/airflow/airflow.db' -e AIRFLOW**CORE**EXECUTOR=SequentialExecutor -it --rm eu.gcr.io/revolut-labs/data-infra/airflow-etl:dev /bin/bash

> docker-compose down --volumes --remove-orphans
> docker-compose -f docker-compose.apache.yml down --volumes --remove-orphans

> d-c -f docker-compose.apache.yml cp airflow-scheduler:/opt/airflow/airflow.cfg airflow.cfg
> d-c -f docker-compose.apache.yml cp airflow-webserver:/opt/airflow/webserver_config.py webserver_config.py

## Airflow

Airflow version 1.10.12.6

> airflow create_user -r Admin -u admin -f Ashish -l malgawa -p test123 -e ashishmalgawa@gmail.com
> airflow create_user -r acquiring-maintainers -u alex -f Alex -l test -p test123 -e alex@gmail.com
> airflow create_user -r User -u john -f John -l Doe -p test123 -e john.doe@revolut.com
> airflow create_user -r User -u tony -f Tony -l Stark -p test123 -e tony.stark@revolut.com

## Python

Python 3.6.14

> > > import sqlalchemy
> > > sqlalchemy.**version**
> > > '1.3.23'

### genereta Models

sqlacodegen postgresql+psycopg2://airflow:airflow@postgres:5432/airflow > ./models.py;
sqlacodegen --noclasses postgresql+psycopg2://airflow:airflow@postgres:5432/airflow > ./tables.py

### Test

- python -m unittest -v rbac_airflow_test.py
- python -m pytest -v --log-cli-level=DEBUG --capture=tee-sys

## Bugs

- LOOK: If the role exists, it will raise an error
  sqlalchemy.exc.IntegrityError: (psycopg2.errors.UniqueViolation) duplicate key value violates unique constraint "ab_role_name_key"
  DETAIL: Key (name)=(data-platform-maintainers) already exists.
