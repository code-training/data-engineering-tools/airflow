-- Set permission to READ ALL DAGS
-- 6: role acquiring-maintainers
-- 131: permission view -> permission 60 = can dag_read && view 57 = all dags

INSERT INTO public.ab_permission_view_role
	VALUES  (9000,131, 6);
    
    
-- Set permission to EDIT specific DAG
-- 6 Role
-- 154 permission can dag edit -> permission 59 = can dag_edit && view 69 = <ex: dag tutorial view>
INSERT INTO public.ab_permission_view_role
	VALUES  (9001,154, 6);
    
    
-- SET Role Viewer to user  
    
INSERT INTO public.ab_user_role(id, user_id, role_id)
VALUES (1000,2, (SELECT id FROM public.ab_role WHERE name ILIKE '%viewer%' LIMIT 1));

--  DELETE "user role" to specific user
DELETE FROM public.ab_user_role
	WHERE 
        role_id=(SELECT id FROM public.ab_role WHERE name ILIKE '%user%' LIMIT 1)
        AND
        user_id=(SELECT id FROM public.ab_user WHERE email= 'alex@gmail.com' LIMIT 1);


-- SELECT ID from permissions can_dag_edit AND can_dag_read
SELECT id FROM public.ab_permission WHERE name ILIKE '%can_dag_edit%' LIMIT 1;
SELECT id FROM public.ab_permission WHERE name ILIKE '%can_dag_read%' LIMIT 1;


-- SELECT ID from dags views USING Regex
SELECT * FROM public.ab_view_menu WHERE name ~* '(?=.*Subdag)(?=.*section)';




-- CREATE SEQUENCES
CREATE SEQUENCE public.test_seq
INCREMENT 1
START 3
MINVALUE 1
MAXVALUE 999999
CACHE 1;