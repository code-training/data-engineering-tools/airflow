-- Get all permissions by default in new role and new user
SELECT 
    r.name,
    pvr.role_id,
    p.name "permission",
    p.id id_permission,
    vm.name "view",
    vm.id id_view,
    pvr.id id_permission_view_role
FROM public.ab_permission_view_role pvr
INNER JOIN public.ab_permission_view pv
    ON pvr.permission_view_id = pv.id
INNER JOIN public.ab_permission p
    ON pv.permission_id = p.id
INNER JOIN public.ab_view_menu vm
    ON pv.view_menu_id = vm.id
INNER JOIN public.ab_role r
    ON pvr.role_id = r.id
WHERE pvr.role_id=6;


