-- Difference between roles


WITH viewer_role AS (
    SELECT id
    FROM public.ab_role
    WHERE name ILIKE '%viewer%'
), user_role AS (
    SELECT id as "id"
    FROM public.ab_role
    WHERE name ILIKE '%user%'
), viewer_permissions AS (
    SELECT *
    FROM public.ab_permission_view_role pvr
    INNER JOIN viewer_role r ON pvr.role_id = r.id
    WHERE role_id = r.id
), user_permissions AS (
    SELECT *
    FROM public.ab_permission_view_role pvr
    INNER JOIN user_role r ON pvr.role_id = r.id
    WHERE role_id = r.id
), diff_permissions AS (
    (
        SELECT
            permission_view_id,
            'not in viewer role' AS note
        FROM
            user_permissions
        EXCEPT
            SELECT
                permission_view_id,
                'not in viewer role' AS note
            FROM
                viewer_permissions
    ) UNION (
    
        SELECT
            permission_view_id,
            'not in user role' AS note
        FROM
            viewer_permissions
        EXCEPT
            SELECT
                permission_view_id,
                'not in user role' AS note
            FROM
                user_permissions   
    )
            

), name_permission AS (
    
    SELECT
        vm.name,
        vm.id,
        d.note
    
    FROM diff_permissions d 
    INNER JOIN ab_view_menu vm ON d.permission_view_id = vm.id

)

SELECT * FROM name_permission;
