from datetime import datetime, timedelta

import airflow
from airflow import DAG, AirflowException
from airflow.exceptions import AirflowFailException
from airflow.operators.python import PythonOperator

from airflow.utils.dates import days_ago
import requests
import pandas as pd


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=30),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}


def get_data(**context):
    page = context.get('page', '1')
    url = f'https://rickandmortyapi.com/api/episode?page={page}'
    print(context)
    req = requests.get(url=url)
    if req.status_code == 200:
        data = req.json()
        results = data.get('results', [])
        if len(results) > 0:
            episodes = [
                {
                    **r,
                    **{'characters': len(r.get('characters', []))}
                } for r in results
            ]

            context.get('ti').xcom_push(key='episodes', value=episodes)

        else:
            # Fail and not retry
            raise AirflowFailException('Result is empty')
    else:
        # Fail and retry
        raise AirflowException(f'Error getting data {req.text}')

    return True


def print_df(**context):
    episodes = context.get('ti').xcom_pull(key='episodes')
    df = pd.DataFrame(episodes)
    print(df.shape)
    print(df.iloc[0])
    return True


with DAG(
    'api_dag',
    default_args=default_args,
    description='ETL using Ricky&Morty API',
    schedule_interval=timedelta(hours=1),
    start_date=days_ago(0),
    catchup=False,
    tags=['api', 'xcom', 'dataframe']
) as dag:

    t1 = PythonOperator(
        task_id="call_api",
        python_callable=get_data,
        op_kwargs={'page': '1'},
    )
    t2 = PythonOperator(
        task_id="show_data",
        python_callable=print_df
    )

    t1 >> t2
