from datetime import datetime, timedelta
from textwrap import dedent

import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.utils.task_group import TaskGroup

from airflow.utils.dates import days_ago


# https://www.youtube.com/watch?v=Q_v6BqS8DhA
# https://www.astronomer.io/guides/task-groups

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=30),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}


def print_log(**context):
    print('Large process here!')
    return True


with DAG(
    'task_group',
    default_args=default_args,
    description='task_group_dag',
    schedule_interval=timedelta(minutes=60),
    start_date=days_ago(2),
    catchup=False,
    tags=['task group'],
) as dag:
    # t1, t2 and t3 are examples of tasks created by instantiating operators
    t1 = BashOperator(
        task_id='print_date',
        bash_command='date',
    )

    with TaskGroup(group_id='parallel_task') as parallel_task:

        parallel_task_t2 = PythonOperator(
            task_id='print',
            python_callable=print_log
        )

        with TaskGroup(group_id='spark_task') as spark_task:
            spark_task_t1 = BashOperator(
                task_id='sleep',
                bash_command='sleep 15'
            )

        with TaskGroup(group_id='flink_task') as flink_task:
            flink_task_t1 = BashOperator(
                task_id='sleep',
                bash_command='sleep 60'
            )

        parallel_task_t2 >> [spark_task, flink_task]

    t3 = BashOperator(
        task_id='echo',
        bash_command='echo "Intermedium task"'
    )
    t4 = BashOperator(
        task_id='print_date_last',
        bash_command='date'
    )

    t1 >> parallel_task >> t3 >> t4
