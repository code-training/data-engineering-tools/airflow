from datetime import datetime, timedelta
from textwrap import dedent
import random

import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

from airflow.utils.dates import days_ago
from airflow.exceptions import AirflowFailException
from airflow.utils.trigger_rule import TriggerRule

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=30),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}



class BaseException(Exception):

    def __init__(self, retry):
        self._retry = retry
        super().__init__()

    @property
    def retry(self):
        return self._retry
    

class PullRequestNotMerged(BaseException):
    pass

class PullRequestDeclined(BaseException):
    pass

def task_retry(context):
    ti = context['task_instance']
    exception = context['exception']

    #print("ti", ti)
    print("exception", exception)
    print("exception", type(exception))
    print(exception.retry)
    if not exception.retry:
        raise AirflowFailException("Opps")
    #print("context", context)

def print_log(**context):
    print(context)

    num = random.randint(0, 50)
    print(num)

    if num > 7:
        raise PullRequestDeclined(retry=False)

    if num == 25:
        print('it is okay')
    else:
    
        raise PullRequestNotMerged(retry=True)
    


with DAG(
    'trigger_rule',
    default_args=default_args,
    description='time_dag',
    schedule_interval=timedelta(minutes=60),
    start_date=days_ago(2),
    catchup=False,
    tags=['time dag'],
) as dag:
    
    t1 = PythonOperator(
        task_id='print_date',
        python_callable=print_log,
        retries= 5,
        #retry_exponential_backoff=True,
        retry_delay= timedelta(seconds=60/2),
        on_retry_callback= task_retry
    )


    success_task = BashOperator(
        task_id='echo_success',
        bash_command='echo "Success"',
        trigger_rule=TriggerRule.ALL_SUCCESS
    )
    failed_task = BashOperator(
        task_id='echo_failed',
        bash_command='echo "Failed"',
        trigger_rule=TriggerRule.ALL_FAILED

    )

    t1 >> [failed_task, success_task]
    
