from datetime import datetime, timedelta
from textwrap import dedent

import airflow
from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python import PythonOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=3),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}


# https://www.youtube.com/watch?v=8uKW0mPWmCk

def task():
    print('Dificult task')


with DAG(
    'master_dag',
    default_args=default_args,
    description='master-dag',
    schedule_interval='@daily',
    # start_date=datetime(2021, 8, 29, 22, 4, 0),
    start_date=days_ago(0),
    catchup=False,
    tags=['master', 'TriggerDagRun'],
) as dag:

    python_task = PythonOperator(
        task_id="python_task",
        python_callable=task
    )

    trigger_task = TriggerDagRunOperator(
        task_id='trigger_task',
        trigger_dag_id='slave_dag',
        execution_date='{{ ds }}',
        reset_dag_run=True,
        wait_for_completion=True
    )

    date_task = BashOperator(
        task_id='date_task',
        bash_command='date',
    )

    python_task >> trigger_task >> date_task
