from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.operators.docker_operator import DockerOperator

# https://towardsdatascience.com/using-apache-airflow-dockeroperator-with-docker-compose-57d0217c8219
# https://github.com/fclesio/airflow-docker-operator-with-compose
default_args = {
    'owner': 'airflow',
    'description': 'Use of the DockerOperator',
    'depend_on_past': False,
    'start_date': datetime(2018, 1, 3),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}
with DAG(
    'docker_dag',
    default_args=default_args,
    schedule_interval="5 * * * *",
    catchup=False,
    tags=['docker_operator']
) as dag:
    t1 = BashOperator(
        task_id='print_current_date',
        bash_command='date'
    )
    t2 = DockerOperator(
        task_id='docker_command',
        image='py_hello_world:1',
        api_version='auto',
        auto_remove=True,
        # command="print app.py",
        # command="/bin/sleep 30",
        docker_url="unix://var/run/docker.sock",
        tty=True,
        environment={
            'PYTHON_ENV': 'dev'
        },
        network_mode="bridge"
    )
    t3 = BashOperator(
        task_id='print_hello',
        bash_command='echo "hello world"'
    )
    t1 >> t2 >> t3
