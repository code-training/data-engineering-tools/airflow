from datetime import datetime, timedelta
from time import sleep
from textwrap import dedent

import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago
from sqlalchemy.sql.sqltypes import PickleType


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=30),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}


def hello_world(ds, **kwargs):
    print(f'Hello {ds} {kwargs}')
    dag_number = kwargs.get('dag_number')
    dag_id = kwargs.get('dag_id')
    print(f'This dag is {dag_number} with id {dag_id}')


def long_process(*args):
    print('Start expensive process')
    sleep(5)
    print('End expensive process')


def create_dag(dag_id, schedule, dag_number, default_args):

    dag = DAG(
        dag_id,
        schedule_interval=schedule,
        catchup=False,
        default_args=default_args,
        tags=['dynamic_dag', 'loop'],
    )

    with dag:
        print_hello = PythonOperator(
            task_id='print_hello',
            python_callable=hello_world,
            op_kwargs={
                'dag_number': dag_number,
                'dag_id': dag_id
            }
        )

        long_task = PythonOperator(
            task_id='long_task',
            python_callable=long_process
        )

        print_hello >> long_task
    return dag


for i in range(5):
    dag_id = f'dynamic_dag_{i}'

    default_args = {
        'owner': 'airflow',
        'start_date': days_ago(0)
    }
    schedule = '@daily'
    globals()[dag_id] = create_dag(
        dag_id=dag_id,
        schedule=schedule,
        dag_number=i,
        default_args=default_args
    )
