from datetime import datetime, timedelta
from textwrap import dedent

import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=3),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}


def _cleaning():
    print('Clearning from target DAG')


with DAG(
    'slave_dag',
    default_args=default_args,
    description='slave-dag',
    schedule_interval='@daily',
    start_date=days_ago(0),
    # start_date=datetime(2021, 8, 29, 22, 2, 0),
    catchup=False,
    tags=['slave', 'TriggerDagRun'],
) as dag:

    sleep_task = BashOperator(
        task_id='sleep_task',
        bash_command='sleep 15',
    )

    cleaning = PythonOperator(
        task_id='cleaning',
        python_callable=_cleaning
    )

    sleep_task >> cleaning
