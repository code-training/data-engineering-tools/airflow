
# iam role for mwaa
resource "aws_iam_role" "role-assume-mwaa" {
  name = "role-airlflow-example-${var.stage}"

  assume_role_policy = file("./files/iam/role_assume_airflow.json")
}
