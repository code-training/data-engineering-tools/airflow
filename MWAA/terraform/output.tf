output "bucket_id" {
  value = aws_s3_bucket.tf_bucket_wmaa_example.id
}

output "bucket_arn" {
  value = aws_s3_bucket.tf_bucket_wmaa_example.arn
}

output "bucket_domain_name" {
  value = aws_s3_bucket.tf_bucket_wmaa_example.bucket_domain_name
}


output "url_airflow" {
  value = aws_mwaa_environment.tf-mwaa-example.webserver_url
}
