data "aws_canonical_user_id" "current_user" {}

resource "aws_s3_bucket" "tf_bucket_wmaa_example" {

  bucket = format("%s%s", "tf-bkt-wmaa-airflow-example", var.stage == "production" ? "" : "-${var.stage}")
  # Allow deletion of non-empty bucket
  force_destroy = true


  tags = {
    Name        = "bucket-airflow-example"
    Environment = var.stage
  }
}

resource "aws_s3_bucket_acl" "tf_bucket_wmaa_acl" {
  bucket = aws_s3_bucket.tf_bucket_wmaa_example.id
  # acl    = "public-read"
  access_control_policy {
    grant {
      grantee {
        id   = data.aws_canonical_user_id.current_user.id
        type = "CanonicalUser"
      }
      permission = "FULL_CONTROL"
    }

    owner {
      id = data.aws_canonical_user_id.current_user.id
    }
  }

}

resource "aws_s3_object" "bucket_object_dags" {
  for_each = fileset("../dags/", "*")
  bucket   = aws_s3_bucket.tf_bucket_wmaa_example.id
  key      = "dags/${each.value}"
  source   = "../dags/${each.value}"
  etag     = filemd5("../dags/${each.value}")
}

resource "aws_s3_object" "bucket_object_scripts" {
  bucket = aws_s3_bucket.tf_bucket_wmaa_example.id
  key    = "scripts/"
}

resource "aws_s3_object" "bucket_object_requirements" {
  bucket = aws_s3_bucket.tf_bucket_wmaa_example.id
  key    = "requirements.txt"
  source = "../requirements.txt"
}

# resource "aws_s3_object" "bucket_object_plugins" {
#   bucket = aws_s3_bucket.tf_bucket_wmaa_example.id
#   key    = "plugins.zip"
#   source = "../plugins.zip"
# }
