

variable "stage" {
  type        = string
  description = "Logical name of the stage, will be used as prefix and in tags (dev/staging/production)"
  default     = "dev"
}

//aws_mwaa_enviroment variable
variable "app_name" {
  description = "MWAA environment name"
  default     = "tf-mwaa-example"
}

variable "region" {
  type        = string
  description = "region"
  default     = "us-west-2"
}

variable "region_az1" {
  type    = string
  default = "us-west-2a"
}
variable "region_az2" {
  type    = string
  default = "us-west-2c"
}

variable "vpc_cidr" {
  type    = string
  default = "10.192.0.0/16"
}
variable "public_subnet1_cidr" {
  type    = string
  default = "10.192.10.0/24"
}
variable "public_subnet2_cidr" {
  type    = string
  default = "10.192.11.0/24"
}
variable "private_subnet1_cidr" {
  type    = string
  default = "10.192.20.0/24"
}
variable "private_subnet2_cidr" {
  type    = string
  default = "10.192.21.0/24"
}
