# vpc for mwaa
resource "aws_vpc" "mwaa-vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = "${var.app_name} vpc"
  }
}

# vpc subnet for mwaa
resource "aws_subnet" "mwaa-private-1" {
  vpc_id            = aws_vpc.mwaa-vpc.id
  cidr_block        = var.private_subnet1_cidr
  availability_zone = var.region_az1
  tags = {
    Name = "${var.app_name} Private Subnet 1"
  }
}

resource "aws_subnet" "mwaa-private-2" {
  vpc_id            = aws_vpc.mwaa-vpc.id
  cidr_block        = var.private_subnet2_cidr
  availability_zone = var.region_az2
  tags = {
    Name = "${var.app_name} Private Subnet 2"
  }
}

resource "aws_subnet" "mwaa-public-1" {
  vpc_id                  = aws_vpc.mwaa-vpc.id
  cidr_block              = var.public_subnet1_cidr
  availability_zone       = var.region_az1
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.app_name} Public Subnet 1"
  }
}

resource "aws_subnet" "mwaa-public-2" {
  vpc_id                  = aws_vpc.mwaa-vpc.id
  cidr_block              = var.public_subnet2_cidr
  availability_zone       = var.region_az2
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.app_name} Public Subnet 2"
  }
}

# internet gateway for mwaa subnet
resource "aws_internet_gateway" "mwaa-inetgw" {
  vpc_id = aws_vpc.mwaa-vpc.id
  tags = {
    Name = "${var.app_name}"
  }
}

# nat gateway for mwaa private subnet
resource "aws_eip" "mwaa-nat1" {
  vpc = true
  tags = {
    Name = "${var.app_name} NAT Gateway 1"
  }
}
resource "aws_eip" "mwaa-nat2" {
  vpc = true
  tags = {
    Name = "${var.app_name} NAT Gateway 2"
  }
}
resource "aws_nat_gateway" "mwaa-nat-gw1" {
  allocation_id = aws_eip.mwaa-nat1.id
  subnet_id     = aws_subnet.mwaa-public-1.id
  depends_on    = [aws_internet_gateway.mwaa-inetgw]
  tags = {
    Name = "${var.app_name} NAT Gateway 1"
  }
}
resource "aws_nat_gateway" "mwaa-nat-gw2" {
  allocation_id = aws_eip.mwaa-nat2.id
  subnet_id     = aws_subnet.mwaa-public-2.id
  depends_on    = [aws_internet_gateway.mwaa-inetgw]
  tags = {
    Name = "${var.app_name} NAT Gateway 2"
  }
}

# route table for mwaa public subnet
resource "aws_route_table" "mwaa-public-route" {
  vpc_id = aws_vpc.mwaa-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.mwaa-inetgw.id
  }
  tags = {
    Name = "${var.app_name} Public Routes"
  }
}
resource "aws_route_table_association" "mwaa-public-route-inetgw1" {
  subnet_id      = aws_subnet.mwaa-public-1.id
  route_table_id = aws_route_table.mwaa-public-route.id
}
resource "aws_route_table_association" "mwaa-public-route-inetgw2" {
  subnet_id      = aws_subnet.mwaa-public-2.id
  route_table_id = aws_route_table.mwaa-public-route.id
}

# route table for mwaa private subnet
resource "aws_route_table" "mwaa-private-route1" {
  vpc_id = aws_vpc.mwaa-vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.mwaa-nat-gw1.id
  }
  tags = {
    Name = "${var.app_name} Private Routes 1"
  }
}
resource "aws_route_table_association" "private-route1-assoc" {
  subnet_id      = aws_subnet.mwaa-private-1.id
  route_table_id = aws_route_table.mwaa-private-route1.id
}

resource "aws_route_table" "mwaa-private-route2" {
  vpc_id = aws_vpc.mwaa-vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.mwaa-nat-gw2.id
  }
  tags = {
    Name = "${var.app_name} Private Routes 2"
  }
}
resource "aws_route_table_association" "private-route2-assoc" {
  subnet_id      = aws_subnet.mwaa-private-2.id
  route_table_id = aws_route_table.mwaa-private-route2.id
}


# security group for mwaa
resource "aws_security_group" "mwaa-execution" {
  name        = "airflow-security-group"
  description = "Security Group for tf-mwaa-example"
  vpc_id      = aws_vpc.mwaa-vpc.id

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = -1
    self      = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}
