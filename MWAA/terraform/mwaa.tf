
# mwaa enviroment
resource "aws_mwaa_environment" "tf-mwaa-example" {
  source_bucket_arn     = aws_s3_bucket.tf_bucket_wmaa_example.arn
  dag_s3_path           = "dags"
  execution_role_arn    = aws_iam_role.role-assume-mwaa.arn
  name                  = format("tf-mwaa-example-%s%s", "${var.app_name}", var.stage == "production" ? "" : "-${var.stage}")
  max_workers           = 2
  webserver_access_mode = "PUBLIC_ONLY"
  network_configuration {
    security_group_ids = [aws_security_group.mwaa-execution.id]
    subnet_ids         = [aws_subnet.mwaa-private-1.id, aws_subnet.mwaa-private-2.id]
  }
  logging_configuration {
    task_logs {
      enabled   = true
      log_level = "INFO"
    }
    webserver_logs {
      enabled   = true
      log_level = "INFO"
    }
    scheduler_logs {
      enabled   = true
      log_level = "INFO"
    }
    worker_logs {
      enabled   = true
      log_level = "INFO"
    }
    dag_processing_logs {
      enabled   = true
      log_level = "INFO"
    }
  }
}
