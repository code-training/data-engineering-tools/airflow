resource "aws_iam_role_policy" "policy-mwaa-exec" {
  name = "policy-mwaa-execution-${var.stage}"
  role = aws_iam_role.role-assume-mwaa.id

  policy = <<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "airflow:PublishMetrics",
        "Resource": "${aws_mwaa_environment.tf-mwaa-example.arn}"
      },
      {
        "Effect": "Deny",
        "Action": [
          "s3:ListAllMyBuckets"
        ],
        "Resource": [
          "${aws_s3_bucket.tf_bucket_wmaa_example.arn}",
          "${aws_s3_bucket.tf_bucket_wmaa_example.arn}/*"
        ]
      },
      {
        "Effect": "Allow",
        "Action": [
          "s3:GetObject*",
          "s3:GetBucket*",
          "s3:List*",
          "s3:*"
        ],
        "Resource": [
          "${aws_s3_bucket.tf_bucket_wmaa_example.arn}",
          "${aws_s3_bucket.tf_bucket_wmaa_example.arn}/*"
        ]
      },
      {
        "Effect": "Allow",
        "Action": [
          "logs:CreateLogStream",
          "logs:CreateLogGroup",
          "logs:PutLogEvents",
          "logs:GetLogEvents",
          "logs:GetLogRecord",
          "logs:GetLogGroupFields",
          "logs:GetQueryResults"
        ],
        "Resource": [
          "arn:aws:logs:${var.region}:*:log-group:airflow-tf-mwaa-example-*"
        ]
      },
      {
        "Effect": "Allow",
        "Action": [
          "logs:DescribeLogGroups"
        ],
        "Resource": [
          "*"
        ]
      },
      {
        "Effect": "Allow",
        "Action": "cloudwatch:PutMetricData",
        "Resource": "*"
      },
      {
        "Effect": "Allow",
        "Action": [
          "sqs:ChangeMessageVisibility",
          "sqs:DeleteMessage",
          "sqs:GetQueueAttributes",
          "sqs:GetQueueUrl",
          "sqs:ReceiveMessage",
          "sqs:SendMessage"
        ],
        "Resource": "arn:aws:sqs:${var.region}:*:airflow-celery-*"
      },
      {
        "Effect": "Allow",
        "Action": [
          "kms:Decrypt",
          "kms:DescribeKey",
          "kms:GenerateDataKey*",
          "kms:Encrypt"
        ],
        "NotResource": "arn:aws:kms:*:*:key/*",
        "Condition": {
          "StringLike": {
            "kms:ViaService": [
              "sqs.${var.region}.amazonaws.com"
            ]
          }
        }
      }
    ]
  }
  EOF

}



