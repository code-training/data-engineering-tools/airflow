# Airflow | Proof Of Concept

# First Basic Tutorial

> https://airflow.apache.org/docs/apache-airflow/stable/tutorial.html

# [Tuan Vu tutorial playlist](https://www.youtube.com/playlist?list=PLYizQ5FvN6pvIOcOd6dFZu3lQqc6zBGp2)

### Repo

> https://github.com/tuanavu/airflow-tutorial

# [Python Decorator](https://www.youtube.com/watch?v=FtBpD-FX1HM)

# Concepts

- ~~Simple DAG~~
- ~~DAG connect with api, raising exceptions and passing values with XCOM~~
- ~~DAG with docker image (DockerOperator)~~
- ~~Trigger DAG from other DAG (TriggerDagRunOperator)~~
- ~~Create dynamics DAGs~~
- ~~Parallel DAGs (TaskGroup)~~

  - How can I include the last task in both target groups?

- Kubernetes Operator
- Airflow hooks
- Spark Operator
- Machine Leraning DAG
- Using decorators
- Pattern with DAGs
-workflow orchestrators (e.g., Airflow) and how to scale them (e.g., Celery, Kubernetes executors, etc)
